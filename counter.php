<?php
$file="stats/stat.log";    // файл для записи истории посещения сайта
$col_zap=4999;    // ограничиваем количество строк log-файла 

// Определяем IP
function getRealIpAddr() { 
  if (!empty($_SERVER['HTTP_CLIENT_IP'])) { 
  $ip=$_SERVER['HTTP_CLIENT_IP']; 
}  elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) { 
	$ip=$_SERVER['HTTP_X_FORWARDED_FOR']; 
}  else { 
	$ip=$_SERVER['REMOTE_ADDR']; 
}
  return $ip;
}

//Выявляем поисковых ботов
if (strstr($_SERVER['HTTP_USER_AGENT'], 'YandexBot')) {
	$bot='YandexBot';
} elseif (strstr($_SERVER['HTTP_USER_AGENT'], 'Googlebot')) {
	$bot='Googlebot';
} else { 
	$bot=explode(' ', $_SERVER['HTTP_USER_AGENT']); $bot=$bot[0];
}

// определяем дату и время события
$ip = getRealIpAddr();
$date = date("d.m.Y / H:i:s");   
 
// определяем страницу сайта    
$home = explode('&', $_SERVER['REQUEST_URI']);    
$home = $home[0];

// определяем referer 
$ref = explode('/', $_SERVER['HTTP_REFERER']); 
$ref = $ref[2];
$lines = file($file);
while(count($lines) > $col_zap) array_shift($lines);
if(!strstr($home, 'knowledge')) { 
	$lines[] = $date."|".$bot."|".$ip."|".$home."|".$ref."\r\n"; 
}
file_put_contents($file, $lines);
?>